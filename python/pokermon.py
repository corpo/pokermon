from flask import Flask, render_template, _app_ctx_stack, url_for, redirect, request, session, Response
from random import randint
import db_manip
from flask_api import status
from flask_cors import CORS
from flask_mail import Mail, Message
from werkzeug.security import generate_password_hash, check_password_hash
import time
import hashlib
import json
import table_manager
import player

#database configuration
DATABASE = '/tmp/pokermon.db'
PER_PAGE = 30
DEBUG = True
SECRET_KEY = 'development key'
USERNAME = 'admin'
PASSWORD = 'default'

app = Flask(__name__,  template_folder='../../pokermon', static_folder = '../../pokermon/assets')
app.config.from_object(__name__)
CORS(app)

app.config['MAIL_SERVER'] = 'smtp.poczta.onet.pl'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = 'pokermon@spoko.pl'
app.config['MAIL_PASSWORD'] = ''
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True
app.config['MAIL_DEFAULT_SENDER'] = 'pokermon@spoko.pl'
mail = Mail(app)

tableManager = table_manager.TableManager()
tableManager.start()
tableManager.addNewTable()
tableManager.addNewTable()
tableManager.addNewTable()
tableManager.addNewTable()

salt = hashlib.sha256('pokermon').hexdigest()

if __name__ == "__main__":
    app.run(debug=True)

def updateChips():
	db = db_manip.get_db()
	cur = db.execute('select chips from user where username=?',[session['username']])
	entry = cur.fetchone()
	session['chips'] = entry[0]

@app.cli.command('initdb')
def initdb_command():
    """Creates the database tables."""
    db_manip.init_db()
    print('Initialized the database.')

	

@app.route("/")
def pokermon():
	if 'username' in session and 'chips' in session and session['username'] and session['chips']:
		return render_template("index.html", login=True, username=session['username'], chips=session['chips'])
	return render_template("index.html", login=False)
     
@app.route("/getTableList", methods=['POST'])
def getTableList():
	result = '{ "result" : ['
	for table in tableManager.tables:
		result += ' { "id" : "' + str(table.id) + '", "players" : '
		table_list = []
		for player in table.players:
			table_list.append(player.username)
		result += json.dumps(table_list) + ' },'
	result = result[:-1]
	result += ' ] }\n'
	return result


@app.route("/getTableDetails", methods=['POST'])
def getTableDetails():
	table = tableManager.getTable(int(request.form['tableId']))
	result = (table.id, table.getPlayers())
	return json.dumps(result)

@app.route("/table/<int:tableId>", methods=['GET'])
def table(tableId):
	if tableManager.tables[tableId].isFull() == False:
		tableManager.tables[tableId].addPlayer(player.Player(session['username'],session['chips']))
		session['table'] = tableId
		return redirect(url_for('pokermon') + '#/table/' + str(tableId))
	else:
		return redirect(url_for('pokermon') + '#/menu') 
	 
		
@app.route('/play')
def rand_play():
    # Selects a random table if not full
	rand_table = randint(0,tableManager.tableCount -1)
	if tableManager.tables[rand_table].isFull() == False:
		tableManager.tables[rand_table].addPlayer(player.Player(session['username'],session['chips']))
		session['table'] = rand_table
		return redirect(url_for('pokermon') + '#/table/' + str(rand_table)) 
	return redirect(url_for('pokermon') + '#/menu') 		
	
	
# PLAYER BUTTONS

@app.route("/table/<int:tableId>/Raise", methods=['POST'])
def Raise(tableId):
	tableManager.tables[tableId].Raise(int(request.form['bet']))
	return redirect(url_for('pokermon') + '#/table/' + str(tableId) )
	
@app.route("/table/<int:tableId>/AllIn", methods=['GET'])
def AllIn(tableId):
	tableManager.tables[tableId].Raise(session['chips'])
	return redirect(url_for('pokermon') + '#/table/' + str(tableId) )	
	
@app.route("/table/<int:tableId>/Check_Call", methods=['GET'])
def Check_Call(tableId):
	tableManager.tables[tableId].Check_Call()
	return redirect(url_for('pokermon') + '#/table/' + str(tableId) )
	
@app.route("/table/<int:tableId>/Fold", methods=['GET'])
def Fold(tableId):
	tableManager.tables[tableId].Fold()
	return redirect(url_for('pokermon') + '#/table/' + str(tableId) )


# TABLE ROUTES

@app.route("/table/<int:tableId>/currentPlayerId", methods=['POST'])
def currentPlayerId(tableId):
	return json.dumps(tableManager.tables[tableId].currentPlayerId == tableManager.tables[tableId].getPlayerId(session['username']))
	
@app.route("/table/<int:tableId>/newHand", methods=['GET'])
def newHand(tableId):
	tableManager.tables[tableId].newHand()
	return redirect(url_for('pokermon') + '#/table/' + str(tableId) ) 
	
	
@app.route("/table/<int:tableId>/nextRound", methods=['GET'])
def nextRound(tableId):
	tableManager.tables[tableId].nextRound()
	return redirect(url_for('pokermon') + '#/table/' + str(tableId) ) 
	
@app.route('/standUp')
def standUp():
    tableManager.tables[session['table']].rmPlayer(session['username'])
    return redirect(url_for('pokermon') + '#/menu') 

@app.route("/table/<int:tableId>/getCards", methods=['POST'])
def getCards(tableId):
	cards = [tableManager.tables[tableId].getPlayerPosition(session['username']), tableManager.tables[tableId].getPlayerCards(tableManager.tables[tableId].getPlayerId(session['username']))]
	try:
		cards[1][0] = cards[1][0].lower()
		cards[1][1] = cards[1][1].lower()
	except IndexError:
		cards[1] = []
	return json.dumps(cards)

@app.route("/table/<int:tableId>/getAllData", methods=['POST'])
def getAllData(tableId):
	updateChips()
	data = tableManager.tables[tableId].allData()
	return json.dumps(data)

@app.route("/table/<int:tableId>/getPot", methods=['POST'])
def getPot(tableId):
	data = tableManager.tables[tableId].pot
	return json.dumps(data)
	

@app.route("/table/<int:tableId>/getTableCards", methods=['POST'])
def getTableCards(tableId):
	cards = tableManager.tables[tableId].getTableCards()
	cards = [x.lower() for x in cards]
	return json.dumps(cards)	

@app.route("/table/<int:tableId>/getTablePlayersExceptYourself", methods=['POST'])	
def getTablePlayersExceptYourself(tableId):
	players = []
	for player in tableManager.tables[tableId].players:
		if player.username != session['username'] and tableManager.tables[tableId].bettingRound == 4:
			players.append((player.username, player.chips, tableManager.tables[tableId].getPlayerCards(tableManager.tables[tableId].getPlayerId(player.username))))
		else:
			if player.username != session['username']:
				players.append((player.username, player.chips))
	return json.dumps(players)	

	
@app.route("/table/<int:tableId>/getTableInfo", methods=['POST'])
def getTableInfo(tableId):
	players = []
	playerId = 0
	for player in tableManager.tables[tableId].players:
		if tableManager.tables[tableId].bettingRound == 4:
		#that is a long line
			players.append((player.username, 
			player.chips, 
			tableManager.tables[tableId].getPlayerPosition(player.username),
			tableManager.tables[tableId].potShare[playerId],
			tableManager.tables[tableId].getPlayerCards(tableManager.tables[tableId].getPlayerId(player.username))))
		else:
		#that is a long line
			players.append((player.username, 
			player.chips, 
			tableManager.tables[tableId].getPlayerPosition(player.username),
			tableManager.tables[tableId].potShare[playerId]
			))
		playerId+=1	
	return json.dumps(players)	

	
@app.route("/table/<int:tableId>/checkWhoWon", methods=['POST'])
def checkWhoWon(tableId):
	return json.dumps(tableManager.tables[tableId].checkWhoWon()) 
	
# LOGGING AND SIGNING
	
@app.route("/signup", methods=['POST'])
def signup():
	# Testing whether username or email already exists in DB
	db = db_manip.get_db()
	login_test = db.execute('select * from user where username=?',[request.form['login']]).fetchone()
	email_test = db.execute('select * from user where email=?',[request.form['email']]).fetchone()
	email_test2 = db.execute('select * from email where new_email=?',[request.form['email']]).fetchone()
	if login_test or email_test or email_test2:
		return redirect(url_for('pokermon')+ '#/signUp')
	# Process of adding a record
	data = [request.form['login'],generate_password_hash(request.form['password']),1000]
	db_manip.add(data)
	# Generate activation code & send confirmation email
	user_id = db.execute('select user_id from user where username=?', [request.form['login']]).fetchone()
	timestamp = int(round(time.time()*1000))
	activation_code = hashlib.sha256(str(user_id[0]) + request.form['email'] + str(timestamp) + salt).hexdigest()
	db.execute('insert into email(user_id,new_email,time,code) values(?,?,?,?)', [user_id[0], request.form['email'], timestamp, activation_code])
	db.commit()
	msg = Message('Account created', recipients = [request.form['email']])
	msg.body = 'Welcome to pokermon, ' + request.form['login'] + '!\n\nHere is your activation link: http://localhost:5000/activation/' + activation_code + '\n\nIt will be valid for the next 24 hours.'
	# TODO cronjob deleting inactive accounts
	mail.send(msg)
	return redirect(url_for('pokermon') + '#/')

@app.route("/activation/<string:activation_code>", methods=['GET'])
def accountActivation(activation_code):
	db = db_manip.get_db()
	entry = db.execute('select user_id,new_email from email where code=?', [activation_code]).fetchone()
	if entry != None:
		db.execute('update user set email=? where user_id=?', [entry[1], entry[0]])
		db.execute('delete from email where user_id=?', [entry[0]])
		db.commit() 
		return redirect(url_for('pokermon') + '#/activation/' + activation_code)
	else:
		return redirect(url_for('pokermon') + '#/')
	
@app.route("/login", methods=['POST'])
def login():
	db = db_manip.get_db()
	entry = db.execute('select username,chips,pw_hash,email from user where username=?', [request.form['login']]).fetchone()
	if entry == None:
		return redirect(url_for('pokermon')+ '#/signUp')
	if entry[3] == None:
		return redirect(url_for('pokermon'))
	if check_password_hash(entry[2], request.form['password']) == False:
		return redirect(url_for('pokermon')+ '#/signUp')
	session['username'] = request.form['login']
	session['chips'] = entry[1]
	return redirect(url_for('pokermon')+ '#/menu')

@app.route('/logout')
def logout():
    # remove the username from the session if it's there
	if 'table' in session:
		tableManager.tables[session['table']].rmPlayer(session['username'])
	session.pop('username', None)
	session.pop('chips', None)
	session.pop('table', None)
	return redirect(url_for('pokermon') + '#/')

@app.route("/getProfileData", methods=['GET'])
def getProfileData():
	if 'username' in session:
		db = db_manip.get_db()
		email = db.execute('select email from user where username=?', [session['username']]).fetchone()
		if email:
			return json.dumps([session['username'], email[0]])
	return None

@app.route("/changeEmail", methods=['POST'])
def changeEmail():
	if 'username' in session:
		db = db_manip.get_db()
		user_id = db.execute('select user_id from user where username=?', [session['username']]).fetchone()
		timestamp = int(round(time.time()*1000))
		activation_code = hashlib.sha256(str(user_id[0]) + request.form['newEmail'] + str(timestamp) + salt).hexdigest()
		db.execute('delete from email where user_id=?', [user_id[0]])
		db.execute('insert into email(user_id,new_email,time,code) values(?,?,?,?)', [user_id[0], request.form['newEmail'], timestamp, activation_code])
		db.commit()
		msg = Message('E-mail address change', recipients = [request.form['newEmail']])
		msg.body = 'Hello, ' + session['username'] + '!\nYou requested to change your e-mail address.\n\nClick on the activation link: http://localhost:5000/changeEmail/' + activation_code + '\n\nIt will be valid for the next 24 hours.'
		mail.send(msg)
		return 'OK'
	return 'ERROR'

@app.route("/changeEmail/<string:activation_code>", methods=['GET'])
def emailChanged(activation_code):
	db = db_manip.get_db()
	entry = db.execute('select user_id,new_email from email where code=?', [activation_code]).fetchone()
	if entry != None:
		db.execute('update user set email=? where user_id=?', [entry[1], entry[0]])
		db.execute('delete from email where user_id=?', [entry[0]])
		db.commit() 
		return redirect(url_for('pokermon') + '#/changeEmail/' + activation_code)
	else:
		return redirect(url_for('pokermon') + '#/')

@app.route("/changePassword", methods=['POST'])
def changePassword():
	if 'username' in session:
		db = db_manip.get_db()
		pw_hash = db.execute('select pw_hash from user where username=?', [session['username']]).fetchone()
		if check_password_hash(pw_hash[0], request.form['oldPassword']) == True:
			db.execute('update user set pw_hash=? where username=?', [generate_password_hash(request.form['newPassword']), session['username']])
			db.commit()
			return 'OK'
	return 'ERROR'

@app.route("/passwordRecovery", methods=['POST'])
def passwordRecovery():
	if request.form['option'] == 'username':
		db = db_manip.get_db()
		user_data = db.execute('select user_id,username,email from user where username=?', [request.form['username']]).fetchone()
		timestamp = int(round(time.time()*1000))
		recovery_code = hashlib.sha256(str(user_data[0]) + user_data[2] + str(timestamp) + salt).hexdigest()
		db.execute('delete from password where user_id=?', [user_data[0]])
		db.execute('insert into password(user_id,time,code) values(?,?,?)', [user_data[0], timestamp, recovery_code])
		db.commit()
		msg = Message('Password recovery', recipients = [user_data[2]])
		msg.body = 'Hello, ' + user_data[1] + '!\nIt looks like you forgot your password.\n\nHere\'s your recovery link: http://localhost:5000/passwordRecovery/' + recovery_code + '\n\nIt will be valid for the next 24 hours.'
		mail.send(msg)
		return 'OK'
	elif request.form['option'] == 'email':
		db = db_manip.get_db()
		user_data = db.execute('select user_id,username,email from user where email=?', [request.form['email']]).fetchone()
		timestamp = int(round(time.time()*1000))
		recovery_code = hashlib.sha256(str(user_data[0]) + user_data[2] + str(timestamp) + salt).hexdigest()
		db.execute('delete from password where user_id=?', [user_data[0]])
		db.execute('insert into password(user_id,time,code) values(?,?,?)', [user_data[0], timestamp, recovery_code])
		db.commit()
		msg = Message('Password recovery', recipients = [user_data[2]])
		msg.body = 'Hello, ' + user_data[1] + '!\nIt looks like you forgot your password.\n\nHere\'s your recovery link: http://localhost:5000/passwordRecovery/' + recovery_code + '\n\nIt will be valid for the next 24 hours.'
		mail.send(msg)
		return 'OK'
	return 'ERROR'

@app.route("/passwordRecovery/<string:recovery_code>", methods=['GET', 'POST'])
def passwordChange(recovery_code):
	db = db_manip.get_db()
	entry = db.execute('select user_id from password where code=?', [recovery_code]).fetchone()
	if entry != None:
		if request.method == 'GET':
			session['userId'] = entry[0]
			return redirect(url_for('pokermon') + '#/passwordRecovery/' + recovery_code)
		else:
			db.execute('update user set pw_hash=? where user_id=?', [generate_password_hash(request.form['newPassword']), session['userId']])
			db.execute('delete from password where user_id=?', [entry[0]])
			db.commit()
			return 'OK'
	else:
		return redirect(url_for('pokermon') + '#/')
