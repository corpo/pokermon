var pokermon =  angular.module('pokermon', ['ngRoute']);

pokermon.config(['$routeProvider', function($routeProvider) {
	$routeProvider
		.when('/', {
				templateUrl: '../assets/templates/main-page.html'
					})
		.when('/signUp', {
				templateUrl: '../assets/templates/sign-up.html'
					})
		.when('/profile', {
				templateUrl: '../assets/templates/profile.html',
				controller: 'profileCtrl'
					})
		.when('/menu', {
				templateUrl: '../assets/templates/menu.html'
					})
		.when('/lobby', {
				templateUrl: '../assets/templates/lobby.html',
				controller: 'lobbyCtrl'
					})
		.when('/table/:tableId', {
				templateUrl: '../assets/templates/table.html',
				controller: 'tableCtrl'
					})
		.when('/activation/:code', {
				templateUrl: '../assets/templates/account-activation.html'
					})
		.when('/changeEmail/:code', {
				templateUrl: '../assets/templates/email-changed.html'
					})
		.when('/forgotPassword', {
				templateUrl: '../assets/templates/forgot-password.html',
				controller: 'forgotPasswordCtrl'
					})
		.when('/passwordRecovery/:code', {
				templateUrl: '../assets/templates/password-recovery.html',
				controller: 'passwordRecoveryCtrl'
					})
		.otherwise({
				redirectTo: '/'
					});
}]);

pokermon.controller('passwordRecoveryCtrl', ['$routeParams', '$http', '$scope', function($routeParams, $http, $scope) {

	$(document).ready(function() {
		$('#alert-password-changed').hide()
		$('#alert-password-mismatch').hide()

		$(document).click(function(e) {
			if(e.target.id == "changePassword") {
				if($('#newPassword').val() == $('#retypeNewPassword').val()) {
					$http({
						method:'POST',
						url:'passwordRecovery/' + $routeParams.code,
						headers : {'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'},
						data: '&newPassword=' + $('#newPassword').val()
					})
					.then(function(resp){
						console.log(resp);
						if(resp.data == 'OK') {
							$('#alert-password-changed').show();
						}
					},function(error){
						console.log(error);
					});
				} else {
					$('#alert-password-mismatch').show();					
				}
			}
		});
	});
}]);

pokermon.controller('forgotPasswordCtrl', ['$http', '$scope', function($http, $scope) {

	$(document).ready(function() {
		$('#alert-recovery-email-sent').hide()

		$(document).click(function(e) {
			if(e.target.id == "recoverPassword") {
				var data = '';
				if($('input[name=option]:checked', '#form').val() == 'username') {
					data = 'option=username&username=' + $('#inputUsername').val();
				}
				else if($('input[name=option]:checked', '#form').val() == 'email') {
					data = 'option=email&email=' + $('#inputEmail').val();
				}

				if(data != '') {
					$http({
							method:'POST',
							url:'passwordRecovery',
							headers : {'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'},
							data: data
						})
						.then(function(resp){
							console.log(resp);
							if(resp.data == 'OK') {
								$('#alert-recovery-email-sent').show();
							}
						},function(error){
							console.log(error);
						});
				}
			}
		});
	});
}]);

pokermon.controller('profileCtrl', ['$http', '$scope', function($http, $scope) {

	$http({
	    method:'GET',
	    url:'getProfileData'
	})
	.then(function(resp){
		console.log(resp);
		$scope.username = resp.data[0].replace(/\"/g, "");
		$scope.email = resp.data[1].replace(/\"/g, "");
	},function(error){
	    console.log(error);
	});

	function hideAllAlerts() {
		$('#alert-wrong-password').hide();
		$('#alert-password-mismatch').hide();
		$('#alert-password-changed').hide();
		$('#alert-email-changed').hide();
	}

	$(document).ready(function() {
		hideAllAlerts();
		$('[data-toggle="modal"]').click(function(e) {
			hideAllAlerts();
		});

		$(document).click(function(e) {
			if(e.target.id == "password-change") {
				if($('#newPassword').val() == $('#retypeNewPassword').val()) {
					$http({
						method:'POST',
						url:'changePassword',
						headers : {'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'},
						data: 'oldPassword=' + $('#oldPassword').val() + '&newPassword=' + $('#newPassword').val()
					})
					.then(function(resp){
						console.log(resp);
						if(resp.data == 'OK') {
							$('#alert-password-changed').show();
							$('#passwordModal').modal('toggle');
						} else if(resp.data == 'ERROR') {
							$('#alert-wrong-password').show();
						}
					},function(error){
						console.log(error);
					});
				} else {
					$('#alert-password-mismatch').show();					
				}
			}
			else if(e.target.id == "email-change" ) {
				$http({
				    method:'POST',
				    url:'changeEmail',
					headers : {'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'},
    				data: 'newEmail=' + $('#newEmail').val()
				})
				.then(function(resp){
					console.log(resp);
					if(resp.data == 'OK') {
						$('#alert-email-changed').show();
					}
				},function(error){
				    console.log(error);
				});
			}
		});
	});
}]);


function checkSeatOccupation(tp, seatNumber) {
	if(tp) {
		for(var i = 0; i < 9 && i < tp.length; i++) {
			if(tp[i][2]+1 == seatNumber) {
				return i;
			}
		}
	}
	return -1;
}

pokermon.controller('lobbyCtrl', ['$http', '$interval', '$scope', function($http, $interval, $scope) {

	var currentTableId = 0;

	$scope.getTableList = function(){     
        $http({
            method:'POST',
            url:'getTableList'
        })
        .then(function(resp){
			$scope.tables = resp.data.result;
        },function(error){
            console.log(error);
        });
    }

	$scope.getTableDetails = function() {
        $http({
            method:'POST',
            url:'getTableDetails',
    		data: 'tableId=' + currentTableId,
			headers : {'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'}
        })
        .then(function(resp){
			$scope.currentTable = resp.data;

			$scope.seat1 = checkSeatOccupation($scope.currentTable[1], 1);
			$scope.seat2 = checkSeatOccupation($scope.currentTable[1], 2);
			$scope.seat3 = checkSeatOccupation($scope.currentTable[1], 3);
			$scope.seat4 = checkSeatOccupation($scope.currentTable[1], 4);
			$scope.seat5 = checkSeatOccupation($scope.currentTable[1], 5);
			$scope.seat6 = checkSeatOccupation($scope.currentTable[1], 6);
			$scope.seat7 = checkSeatOccupation($scope.currentTable[1], 7);
			$scope.seat8 = checkSeatOccupation($scope.currentTable[1], 8);
			$scope.seat9 = checkSeatOccupation($scope.currentTable[1], 9);
        },function(error){
            console.log(error);
        });
    }

	$scope.setCurrentTable = function(tableId) {
		currentTableId = tableId;
		$scope.getTableDetails();
	}

	$scope.selectTable = function(tableId) {
		$http({
		    method:'GET',
		    url:'table/' + tableId
		})
		.then(function(resp){
			console.log(resp);
		},function(error){
		    console.log(error);
		});
	}

	$scope.getCardUrl = function(card) {
		return "assets/img/cards/" + card + ".png";
	}

	$scope.tableCards = function(){     
        $http({
            method:'POST',
            url:'table/' + currentTableId + '/getTableCards'
        })
        .then(function(resp){
			console.log(resp);
			$scope.tablecards = resp.data;
        },function(error){
            console.log(error);
        });
    }
	
	$scope.getTableList();
	$scope.getTableDetails();
	var tableListPromise = $interval($scope.getTableList, 1000);
	var currentTablePromise = $interval($scope.getTableDetails, 3000);
	var tableCardsPromise = $interval($scope.tableCards, 1000);

	$scope.$on('$destroy', function() {
		$interval.cancel(tableListPromise);
		$interval.cancel(currentTablePromise);
		$interval.cancel(tableCardsPromise);
	});
}]);

pokermon.controller('tableCtrl', ['$interval', '$http', '$scope', '$routeParams', function($interval, $http, $scope, $routeParams) {

	$scope.tableId = $routeParams.tableId;
	
	$scope.playerCards = function(){     
        $http({
            method:'POST',
            url:'table/' + $scope.tableId + '/getCards'
        })
        .then(function(resp){


			$scope.cards = resp.data;

        },function(error){
            console.log(error);
        });
    }
	
	
	$scope.tableCards = function(){     
        $http({
            method:'POST',
            url:'table/' + $scope.tableId + '/getTableCards'
        })
        .then(function(resp){

			$scope.tablecards = resp.data;

        },function(error){
            console.log(error);
        });
    }

	$scope.tablePlayers = function(){     
        $http({
            method:'POST',
            url:'table/' + $scope.tableId + '/getTableInfo'
        })
        .then(function(resp){
			$scope.tableplayers = resp.data;


			$scope.seat1 = checkSeatOccupation($scope.tableplayers, 1);
			$scope.seat2 = checkSeatOccupation($scope.tableplayers, 2);
			$scope.seat3 = checkSeatOccupation($scope.tableplayers, 3);
			$scope.seat4 = checkSeatOccupation($scope.tableplayers, 4);
			$scope.seat5 = checkSeatOccupation($scope.tableplayers, 5);
			$scope.seat6 = checkSeatOccupation($scope.tableplayers, 6);
			$scope.seat7 = checkSeatOccupation($scope.tableplayers, 7);
			$scope.seat8 = checkSeatOccupation($scope.tableplayers, 8);
			$scope.seat9 = checkSeatOccupation($scope.tableplayers, 9);
        },function(error){
            console.log(error);
        });
    }
	
	$scope.whoWon = function(){     
        $http({
            method:'POST',
            url:'table/' + $scope.tableId + '/checkWhoWon'
        })
        .then(function(resp){
			console.log(resp);
        },function(error){
            console.log(error);
        });
    }
	
	$scope.currentPlayerId = function(){     
        $http({
            method:'POST',
            url:'table/' + $scope.tableId + '/currentPlayerId'
        })
        .then(function(resp){
			$scope.currentPlayerId = resp.data;
        },function(error){
            console.log(error);
        });
    }
	
	$scope.allData = function(){     
        $http({
            method:'POST',
            url:'table/' + $scope.tableId + '/getAllData'
        })
        .then(function(resp){
			console.log(resp.data);
        },function(error){
            console.log(error);
        });
    }
		
	$scope.potData = function(){     
        $http({
            method:'POST',
            url:'table/' + $scope.tableId + '/getPot'
        })
        .then(function(resp){
			console.log(resp.data);
			$scope.pot = resp.data;
        },function(error){
            console.log(error);
        });
    }
	
	var promise = $interval($scope.playerCards, 1000);
	var promise2 = $interval($scope.tableCards, 1000);
	var promise3 = $interval($scope.tablePlayers, 1000);
	var promise4 = $interval($scope.whoWon, 1000);
	var promise5 = $interval($scope.currentPlayerId, 1000);
	var promise6 = $interval($scope.allData, 1000);
	var promise6 = $interval($scope.potData, 1000);
	$scope.$on('$destroy', function() {
		$interval.cancel(promise);
		$interval.cancel(promise2);
		$interval.cancel(promise3);
		$interval.cancel(promise4);
		$interval.cancel(promise5);
		$interval.cancel(promise6);
		$interval.cancel(promise7);
	});

	$(document).ready(function() {
		$('[data-toggle="popover"]').popover();
		$(document).click(function(e) {
		     if(e.target.id == "popover-close" )
		     {
		          $('#raise').popover('hide');                
		     }

		  });
	});

	$scope.getCardUrl = function(card) {
		return "assets/img/cards/" + card + ".png";
	}

	$scope.getCard = function(position, number) {
		var seat = checkSeatOccupation($scope.tableplayers, position);
		if(seat == -1) {
			return null;
		}
		else {
			if($scope.tableplayers[seat].length >= 5) {
				return $scope.getCardUrl($scope.tableplayers[seat][4][number]);
			}
			else {
				if(position == $scope.cards[0]+1) {
					return $scope.getCardUrl($scope.cards[1][number]);
				}
				else {
					return $scope.getCardUrl("back");
				}
			}
		}
	}
}]);
