drop table if exists user;
create table user (
	user_id integer primary key autoincrement,
	username text not null,
	email text,
	pw_hash text not null,
	chips integer
);

drop table if exists email;
create table email (
	id integer primary key autoincrement,
	user_id integer not null,
	new_email text not null,
	time integer not null,
	code text not null
);

drop table if exists password;
create table password (
	id integer primary key autoincrement,
	user_id integer not null,
	time integer not null,
	code text not null
);
