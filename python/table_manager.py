import threading
import table
import time

class TableManager(threading.Thread):

	def __init__(self):
		threading.Thread.__init__(self)
		self.tableCount = 0
		self.tables = []
		self.addNewTable()

	def addNewTable(self):
		self.tables.append(table.Table(self.tableCount ))
		self.tableCount += 1

	def run(self):
		for table in self.tables:
			if table.isFull():
				self.addNewTable()
		time.sleep(1)

	def getTable(self, id):
		for table in self.tables:
			if table.id == id:
				return table
		return None
