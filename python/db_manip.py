from sqlite3 import dbapi2 as sqlite3
from flask import _app_ctx_stack
DATABASE = './tmp/pokermon.db'
	
def get_db():
    """Opens a new database connection if there is none yet for the
    current application context.
    """
    top = _app_ctx_stack.top
    if not hasattr(top, 'sqlite_db'):
        top.sqlite_db = sqlite3.connect(DATABASE)
        top.sqlite_db.row_factory = sqlite3.Row
    return top.sqlite_db

def init_db():
    """Initializes the database."""
    db = get_db()
    with open('../schema.sql', mode='r') as f:
        db.cursor().executescript(f.read())
    db.commit()


def add( values ):
	'''
	INPUT - list of three values: username, pw_hash, chips
	OUTPUT - record added to database
	'''
	db = get_db()
	db.execute('insert into user(username, pw_hash, chips) values (?, ?, ?)', values)
	db.commit()

''' TO DO	
def check(columns, records, values)
	
	INPUT - list of columns,  records (A,B,C,..) and  values (a,b,c,...), last two lists of same length
	OUTPUT - rows from database matching the query(A==a, B==b, C==c,...) with selected columns 
	
	if len(records) == len(values):
		db = get_db()
		cur = db.execute()
'''
