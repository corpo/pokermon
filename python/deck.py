import random
class Deck:

	def __init__(self):
		self.cardCount = 52
		colors = 'cdhs'
		ranks = 'A23456789TJQK'
		self.order = []
		for color in colors:
			for rank in ranks:
				self.order.append(rank + color)
		random.shuffle(self.order)

	def show(self):
		return self.order
		
	def next(self):
		next = self.order[0]
		self.order = self.order[1:] + self.order[:1]
		return next
		
	def shuffle(self):
		random.shuffle(self.order)
