import player
import deck
import threading
import re
import itertools
import db_manip

class Table(threading.Thread):

	def __init__(self, id):
		threading.Thread.__init__(self)
		self.players = []
		self.id = id
		self.playerCount = 0
		self.deck = deck.Deck()
		self.currentDealerId = 0
		self.currentPlayerId = (self.currentDealerId +3) % max(self.playerCount,1)
		self.active = []
		self.madeAMove = []
		self.positions = []
		self.pot = 0
		self.potShare = []
		self.maxBet = 0
		self.cards = self.getCards()
		self.tableCards = []
		self.bettingRound = 0 #  0 - preflop ; 1 - flop ; 2 - turn ; 3 - river
		
	def allData(self):
		data = []
		data.append(self.id)
		data.append(self.playerCount)
		data.append(self.currentDealerId)
		data.append(self.currentPlayerId )
		data.append(self.active )
		data.append(self.madeAMove)
		data.append(self.positions)
		data.append(self.pot)
		data.append(self.potShare)
		data.append(self.maxBet)
		data.append(self.cards)
		data.append(self.tableCards)
		data.append(self.bettingRound)
		return data
	def choosePosition(self):
		avalPositions = [0,1,2,3,4,5,6,7,8]
		if self.positions == []:
			return 0
		for position in self.positions:
			avalPositions.pop(avalPositions.index(position))
		return avalPositions[0]
	
	def addPlayer(self, player):
		if self.isFull():
			return False
		else:
			self.playerCount+=1
			self.active.append(False)
			self.madeAMove.append(False)
			self.positions.append(self.choosePosition())
			self.players.append(player)
			self.potShare.append(0)
			return True
	
	
	
	def getPlayerId(self,username):
		playerId = 0 
		for player in self.players:
			if player.username == username:
				return playerId
			playerId+=1
			
	def getPlayerPosition(self,username):
		playerId = 0 
		for player in self.players:
			if player.username == username:
				return self.positions[playerId]
			playerId+=1		
			
	def getPlayers(self):
		result = []
		for player in self.players:
			result.append((player.username, player.chips, self.getPlayerPosition(player.username)))
		return result

	def rmPlayer(self,username):
		self.updateScores()
		playerId = 0 
		for player in self.players:
			if player.username == username:
				self.players.remove(player)
				self.potShare.pop(playerId)
				self.active.pop(playerId)
				self.madeAMove.pop(playerId)
				self.positions.pop(playerId)
				self.playerCount-=1
			playerId+=1
	
	def isFull(self):
		return self.playerCount > 9
		
	def newHand(self):
		self.active = []
		self.madeAMove = []
		self.potShare = []
		self.tableCards = []
		self.maxBet = 0
		self.currentDealerId = (self.currentDealerId + 1) % self.playerCount
		self.currentPlayerId = (self.currentDealerId +3) % self.playerCount
		self.pot = 0
		self.bettingRound = 0
		self.deck.shuffle()
		for player in self.players:
			self.active.append(True)
			self.madeAMove.append(False)
			self.potShare.append(0)
		self.cards = self.getCards()
	
	def getCards(self):
		cards = []
		for player in self.players:
			cards.append([self.deck.next(),self.deck.next()])
		return cards
		
	def getPlayerCards(self,id):
		try:
			cards = self.cards[id]
		except IndexError:
			cards = None
		return cards
		
	def getTableCards(self):
		return self.tableCards
	
	#TABLE BUTTONS FUNCTIONS
	
	def changePlayer(self):
		self.madeAMove[self.currentPlayerId] = True
		try:
			self.currentPlayerId += 1
			while self.active[self.currentPlayerId]!=True:
				self.currentPlayerId += 1
				if self.currentPlayerId == self.playerCount:
					self.currentPlayerId = 0
		except IndexError:
			self.currentPlayerId = 0	
			while self.active[self.currentPlayerId]!=True:
				self.currentPlayerId += 1
		if self.checkForNextRound() == True:
			self.calculatePot()
			self.nextRound()
		if self.checkForNewHand() == True:	
			self.calculatePot()
			self.winner(self.currentPlayerId)
			self.updateScores()
			self.newHand()
	
	
	def Raise(self,bet):
		assert bet > (self.maxBet - self.potShare[self.currentPlayerId]) , 'Bet too low'
		self.players[self.currentPlayerId].chips -= bet		
		self.maxBet = self.potShare[self.currentPlayerId] + bet
		self.potShare[self.currentPlayerId] = self.maxBet
		self.anotherMove()	
		self.changePlayer()		

		
	def updateScores(self):
		db = db_manip.get_db()
		for player in self.players:
			db.execute('update user  set chips=? where username=?',[player.chips, player.username])
			db.commit()
		
		
		
	def anotherMove(self):
		self.madeAMove = []
		for move in self.active:
			self.madeAMove.append( not move)
			
			
	def Fold(self):
		self.active[self.currentPlayerId] = False
		self.changePlayer()

		
	def Check_Call(self):
		self.players[self.currentPlayerId].chips -= (self.maxBet - self.potShare[self.currentPlayerId])
		self.potShare[self.currentPlayerId] = self.maxBet
		self.changePlayer()
		
	def calculatePot(self):
		self.pot = 0
		for cash in self.potShare:
			self.pot += cash
	
	def winner(self,ids):
		potPerPlayer = self.pot/len(ids)
		for id in  ids:
			self.players[id].chips += potPerPlayer
		
	
	#ENDS TURN
	
	def checkForNextRound(self):
		for move in self.madeAMove:
			if move == False:
				return False
		return True		
		
		
	def checkForNewHand(self):
		amountActive = 0
		for active in self.active:
			if active == True:
				amountActive+=1
		if amountActive == 1:
			return True
		return False	
		
	def checkWhoWon(self):
		if self.bettingRound == 4:	
			handsToCheck = []
			idToCheck = []
			winnerId= []
			checkedId = 0
			for player in self.players:
				if self.active[checkedId] == True:
					handsToCheck.append(self.cards[checkedId])
					idToCheck.append(checkedId)	
				checkedId+=1
			winners = self.whoWon(handsToCheck,self.tableCards)
			for id in winners:
				winnerId.append(idToCheck[id])
			return winnerId
		return []


	
		
	def nextRound(self):
		if self.bettingRound == 0:
			self.tableCards.append(self.deck.next())
			self.tableCards.append(self.deck.next())
			self.tableCards.append(self.deck.next())
			self.anotherMove()
		if self.bettingRound == 1 or self.bettingRound == 2:
			self.tableCards.append(self.deck.next())
			self.anotherMove()
		if self.bettingRound > 3:
			winnerId = self.checkWhoWon()
			self.winner(winnerId)
			self.newHand()
			return None
			
		self.bettingRound+=1
		
	def getPlayer(self,id):
		player = []
		player.append(self.players[id])
		player.append(getPlayerCards(id))
		return player
	

	def whoWon(self,playerCards, tableCards):
		playersPossibleHands = []
		playersStrongestHand = []
		playerId = []
		id = 0
		for cards in playerCards:
			playersPossibleHands.append(list(itertools.combinations([cards[0],cards[1], tableCards[0],tableCards[1],tableCards[2],tableCards[3],tableCards[4]],5)))
		for hands in playersPossibleHands:
			playersStrongestHand.append(self.chooseStrongestHand(hands)[0])
		strongestHand = self.chooseStrongestHand(playersStrongestHand)[0]
		for hand in playersStrongestHand:
			if self.compareHand(hand,strongestHand) == 0:
				playerId.append(id)
			id += 1
		return playerId
		
	def checkHand(self,cards):
		'''
		INPUT
		cards - 5 cards
		OUTPUT
		returned value is a number that indicates the strength of the hand
		'''
		ranks = []
		for card in cards:
			ranks.append(card[0])
		ranks = ''.join(sorted(ranks)) 
		if cards[1][1]==cards[2][1]==cards[3][1]==cards[4][1]==cards[0][1]: # AKA checkFlush
			if self.checkStraight(ranks):
				return 8 # Straight flush 
			else:
				return 5 # Flush
		if self.checkStraight(ranks):
			return 4 # Straight
		if re.search('(.)\\1\\1\\1',ranks):
			return 7 # Four of a kind
		if re.search('(.)\\1\\1(.)\\2',ranks) or re.search('(.)\\1(.)\\2\\2',ranks):
			return 6 # Full house	
		if re.search('(.)\\1\\1',ranks):
			return 3 # Three of a kind
		if re.search('(.)\\1.*(.)\\2',ranks):
			return 2 # Two pair
		if re.search('(.)\\1',ranks):
			return 1 # One pair
		return 0 # High card
		
	def checkStraight(self,ranks):
		possibilities = ['2345A','23456','34567','45678','56789','6789T','789JT','89JQT','9JKQT','AJKQT']
		for possibility in possibilities:
			if ranks == possibility:
				return True
		return False		
		
	def chooseStrongestHand(self,hands):
		'''
		INPUT 
		list of hands
		OUTPUT
		strongest hand
		'''
		strongest = None
		for hand in hands:
			if strongest == None:
				strongest = [hand]
				continue
			if self.checkHand(hand) > self.checkHand(strongest[0]):
				strongest = [hand]
				continue
			if self.checkHand(hand) == self.checkHand(strongest[0]):
				if self.compareHand(hand,strongest[0]) == 1:
					strongest = [hand]
					continue
				if self.compareHand(hand,strongest[0]) == 0:
					strongest.append(hand)
					continue
		return strongest			
		
		
	def compareHand(self,hand1,hand2):
		'''
		INPUT
		two hands 
		OUTPUT
		1 - hand1 is stronger
		-1 - hand2 is stronger
		0 - hand1 is equal to hand2
		'''
		# Trivial cases of different categories
		
		if self.checkHand(hand1) > self.checkHand(hand2):
			return 1
		if self.checkHand(hand1) < self.checkHand(hand2):
			return -1
			
		# I am translating the cards so that they are in order of strength when sorted alphabetically
		
		translation_table = {ord('T'): ord('A'), ord('J'): ord('B'),ord('Q'): ord('C'),ord('K'): ord('D'),ord('A'): ord('E')}
		ranks1 = []
		for card in hand1:
			ranks1.append(card[0])
		ranks1 = ''.join(sorted(ranks1)) 
		ranks1 = ranks1.translate(translation_table)
		ranks1 =  ''.join(sorted(ranks1))
		ranks2 = []
		for card in hand2:
			ranks2.append(card[0])
		ranks2 = ''.join(sorted(ranks2)) 
		ranks2 = ranks2.translate(translation_table)
		ranks2 =  ''.join(sorted(ranks2))
		# Tie breaking depending on the hand category
		category = self.checkHand(hand1)
		# Only possible tie
		if ranks1 == ranks2:
			return 0
		# Straight flushes and straights	
		if category == 8 or category == 4:
			# Smallest possible straight
			if ranks1 == '2345E':
				return -1
			if ranks2 == '2345E':
				return 1
			# Highest card decides
			if ord(ranks1[4])>ord(ranks2[4]):
				return 1
			return -1
		# Flush or high card - highest different card decides
		if category == 5 or category == 0:
			order = [4,3,2,1,0]
			for i in order:
				if ord(ranks1[i]) > ord(ranks2[i]):
					return 1
				if ord(ranks1[i]) < ord(ranks2[i]):
					return -1
		# Four of a kind - first we check the four, then the remaining card
		if category == 7:
			result1 = re.search('(.)\\1\\1\\1',ranks1)
			result2 = re.search('(.)\\1\\1\\1',ranks2)
			if ord(result1.group(0)[0]) > ord(result2.group(0)[0]):
				return 1
			if ord(result1.group(0)[0]) < ord(result2.group(0)[0]):
				return -1
			ranks1 = ranks1.replace(result1.group(0),'')
			ranks2 = ranks2.replace(result2.group(0),'')
			if ord(ranks1) > ord(ranks2):
				return 1
			return -1
		# Three of a kind - first we check the three, then the remaining cards
		if category == 3:
			result1 = re.search('(.)\\1\\1',ranks1)
			result2 = re.search('(.)\\1\\1',ranks2)
			if ord(result1.group(0)[0]) > ord(result2.group(0)[0]):
				return 1
			if ord(result1.group(0)[0]) < ord(result2.group(0)[0]):
				return -1
			ranks1 = ranks1.replace(result1.group(0),'')
			ranks2 = ranks2.replace(result2.group(0),'')
			order = [1,0]
			for i in order:
				if ord(ranks1[i]) > ord(ranks2[i]):
					return 1
				if ord(ranks1[i]) < ord(ranks2[i]):
					return -1
		
		# One pair - first we check the pair, then the remaining cards
		if category == 1:
			result1 = re.search('(.)\\1',ranks1)
			result2 = re.search('(.)\\1',ranks2)
			if ord(result1.group(0)[0]) > ord(result2.group(0)[0]):
				return 1
			if ord(result1.group(0)[0]) < ord(result2.group(0)[0]):
				return -1
			ranks1 = ranks1.replace(result1.group(0),'')
			ranks2 = ranks2.replace(result2.group(0),'')
			order = [2,1,0]
			for i in order:
				if ord(ranks1[i]) > ord(ranks2[i]):
					return 1
				if ord(ranks1[i]) < ord(ranks2[i]):
					return -1
					
		# Two pair - first we check the bigger pair, then the smaller one, then the remaining card
		if category == 2:
			result1 = re.search('(.)\\1.*(.)\\2',ranks1)
			result2 = re.search('(.)\\1.*(.)\\2',ranks2)
			if max(ord(result1.group(1)[0]),ord(result1.group(2)[0])) \
			> max(ord(result2.group(1)[0]),ord(result2.group(2)[0])):
				return 1
			if  max(ord(result1.group(1)[0]),ord(result1.group(2)[0])) \
			< max(ord(result2.group(1)[0]),ord(result2.group(2)[0])):
				return -1
			if min(ord(result1.group(1)[0]),ord(result1.group(2)[0])) \
			> min(ord(result2.group(1)[0]),ord(result2.group(2)[0])):
				return 1
			if min(ord(result1.group(1)[0]),ord(result1.group(2)[0])) \
			< min(ord(result2.group(1)[0]),ord(result2.group(2)[0])):
				return -1	
			ranks1 = ranks1.replace(result1.group(1),'')
			ranks1 = ranks1.replace(result1.group(2),'')
			ranks2 = ranks2.replace(result2.group(1),'')
			ranks2 = ranks2.replace(result2.group(2),'')
			if ord(ranks1) > ord(ranks2):
				return 1
			return -1		
		
		# Full house - first we check the three, then  the two
		if category == 6:
			# types:
			# 1 - three then pair
			# 2 - pair then three
			type1 = 1
			type2 = 1
			result1 = re.search('(.)\\1\\1(.)\\2',ranks1) 
			if result1 == None:
				result1 = re.search('(.)\\1(.)\\2\\2',ranks1)
				type1 = 2
			result2 = re.search('(.)\\1\\1(.)\\2',ranks2) 
			if result2 == None:
				result2 = re.search('(.)\\1(.)\\2\\2',ranks2)
				type2 = 2
			if ord(result1.group(type1)[0]) > ord(result2.group(type2)[0]):
				return 1
			if ord(result1.group(type1)[0]) < ord(result2.group(type2)[0]):
				return -1

			ranks1 = ranks1.replace(result1.group(type1),'')
			ranks2 = ranks2.replace(result2.group(type2),'')
			if ord(ranks1[0]) > ord(ranks2[0]):
				return 1
			return -1
